import { Book } from './../../app/pages/audiobooks/litres.model';
import { AUDIOBOOKS_ACTION, AudiobooksAction } from './audiobooks.action';

export interface State {
  books: Book[];
}

const initialState: State = {
  books: []
};

export function AudiobooksReducer(state = initialState, action: AudiobooksAction) {
  switch (action.type) {
    case AUDIOBOOKS_ACTION.LOAD_BOOKS:
      state = initialState;
      return {
        ...state,
        books: [...action.books]
      };
    case AUDIOBOOKS_ACTION.PRELOAD_BOOKS:
      return {
        ...state,
        books: [...state.books, ...action.books]
      };
    default:
      return state;
  }
}
