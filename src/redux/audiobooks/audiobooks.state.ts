import { Book } from './../../app/pages/audiobooks/litres.model';

export interface AudiobooksState {
    audiobooksData: {
        books: Book[]
    };
}
