import { Action } from '@ngrx/store';
import { FavoriteChannel, FavoriteVideo, FavoriteTvshow, FavoriteBook } from '@models/core';


// tslint:disable-next-line: no-namespace
export namespace FAVORIE_ACTION {
  export const LOAD_FAVORITE_CHANNELS = 'LOAD_FAVORITE_CHANNELS';
  export const LOAD_FAVORITE_VIDEOS = 'LOAD_FAVORITE_VIDEOS';
  export const LOAD_FAVORITE_TVSHOWS = 'LOAD_FAVORITE_TVSHOWS';
  export const LOAD_FAVORITE_BOOKS = 'LOAD_FAVORITE_BOOKS';

  export const ADD_FAVORITE_CHANNEL = 'ADD_FAVORITE_CHANNEL';
  export const ADD_FAVORITE_VIDEO = 'ADD_FAVORITE_VIDEO';
  export const ADD_FAVORITE_TVSHOW = 'ADD_FAVORITE_TVSHOW';
  export const ADD_FAVORITE_BOOK = 'ADD_FAVORITE_BOOK';

  export const DELETE_FAVORITE_CHANNEL = 'DELETE_FAVORITE_CHANNEL';
  export const DELETE_FAVORITE_VIDEO = 'DELETE_FAVORITE_VIDEO';
  export const DELETE_FAVORITE_TVSHOW = 'DELETE_FAVORITE_TVSHOW';
  export const DELETE_FAVORITE_BOOK = 'DELETE_FAVORITE_BOOK';

}

export class LoadFavoriteChannels implements Action {
  readonly type = FAVORIE_ACTION.LOAD_FAVORITE_CHANNELS;
  constructor(public channels: FavoriteChannel[]) { }
}

export class LoadFavoriteVideos implements Action {
  readonly type = FAVORIE_ACTION.LOAD_FAVORITE_VIDEOS;

  constructor(public videos: FavoriteVideo[]) { }
}

export class LoadFavoriteTvshows implements Action {
  readonly type = FAVORIE_ACTION.LOAD_FAVORITE_TVSHOWS;

  constructor(public tvshows: FavoriteTvshow[]) { }
}

export class LoadFavoriteBooks implements Action {
  readonly type = FAVORIE_ACTION.LOAD_FAVORITE_BOOKS;

  constructor(public books: FavoriteBook[]) { }
}

export class DeleteFavoriteChannel implements Action {
  readonly type = FAVORIE_ACTION.DELETE_FAVORITE_CHANNEL;

  constructor(public id: number) { }
}

export class AddFavoriteChannel implements Action {
  readonly type = FAVORIE_ACTION.ADD_FAVORITE_CHANNEL;

  constructor(public channel: FavoriteChannel) { }
}

export class DeleteFavoriteTvshow implements Action {
  readonly type = FAVORIE_ACTION.DELETE_FAVORITE_TVSHOW;

  constructor(public id: string) { }
}

export class AddFavoriteTvshow implements Action {
  readonly type = FAVORIE_ACTION.ADD_FAVORITE_TVSHOW;

  constructor(public tvshow: FavoriteTvshow) { }
}

export class DeleteFavoriteVideo implements Action {
  readonly type = FAVORIE_ACTION.DELETE_FAVORITE_VIDEO;

  constructor(public id: number) { }
}

export class AddFavoriteVideo implements Action {
  readonly type = FAVORIE_ACTION.ADD_FAVORITE_VIDEO;

  constructor(public video: FavoriteVideo) { }
}

export class DeleteFavoriteBook implements Action {
  readonly type = FAVORIE_ACTION.DELETE_FAVORITE_BOOK;

  constructor(public id: number) { }
}

export class AddFavoriteBook implements Action {
  readonly type = FAVORIE_ACTION.ADD_FAVORITE_BOOK;

  constructor(public book: FavoriteBook) { }
}


export type FavoriteAction =
  LoadFavoriteChannels  |
  LoadFavoriteVideos    |
  LoadFavoriteTvshows   |
  LoadFavoriteBooks     |
  DeleteFavoriteChannel |
  AddFavoriteChannel    |
  DeleteFavoriteTvshow  |
  AddFavoriteTvshow     |
  DeleteFavoriteVideo   |
  AddFavoriteVideo      |
  DeleteFavoriteBook    |
  AddFavoriteBook       ;
