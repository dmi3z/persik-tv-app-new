import { PlayerModule } from './components/player/player.module';
import { ConnectionInfoComponent } from './components/connection-info/connection-info.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { LoopingRhumbusesSpinnerModule } from 'angular-epic-spinners';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';

import { ChannelsReducer } from '../redux/channels/channels.reducer';
import { NavModule } from './directives/nav.module';
import { ExitModalComponent } from './components/exit-modal/exit-modal.component';

import {
  TimeService,
  ImageService,
  DataService,
  AuthService,
  BackService,
  MenuControlService,
  ParamInterceptor,
  AdultService,
  LoaderService,
  PlayerControllerService,
  PositionMemoryService
} from '@services/core';

import { VodReducer } from 'src/redux/vod/vod.reducer';
import { favoritesReducer } from 'src/redux/favorite/favorite.reducer';
import { FavoriteService } from './services/favorite.service';
import { AudiobooksReducer } from 'src/redux/audiobooks/audiobooks.reducer';
import { GenreFilterModule } from './components/genre-filter/genre-filter.module';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    ExitModalComponent,
    ConnectionInfoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpClientModule,
    LoopingRhumbusesSpinnerModule,
    GenreFilterModule,
    StoreModule.forRoot(
      {
        channelsData: ChannelsReducer,
        vodCategories: VodReducer,
        favoritesData: favoritesReducer,
        audiobooksData: AudiobooksReducer
      }),
    FormsModule,
    NavModule,
    PlayerModule
  ],
  providers: [
    AuthService,
    DataService,
    ImageService,
    TimeService,
    LoaderService,
    AdultService,
    FavoriteService,
    PlayerControllerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true
    },
    MenuControlService,
    BackService,
    PositionMemoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
