import { FavoriteBook } from './../models/favorite';
import {
  LoadFavoriteChannels,
  LoadFavoriteVideos,
  LoadFavoriteTvshows,
  DeleteFavoriteChannel,
  DeleteFavoriteTvshow,
  DeleteFavoriteVideo,
  AddFavoriteChannel,
  AddFavoriteTvshow,
  AddFavoriteVideo,
  LoadFavoriteBooks,
  AddFavoriteBook,
  DeleteFavoriteBook
} from './../../redux/favorite/favorite.action';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FavoriteChannel, FavoriteVideo, FavoriteTvshow, ContentType, ContentName } from '@models/core';
import { Store } from '@ngrx/store';
import { FavoritesState } from 'src/redux/favorite/favorite.state';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoaderService } from './loader.service';

@Injectable()

export class FavoriteService {

  private BASE_URL = 'https://api.persik.by/';

  constructor(private http: HttpClient, private favoriteStore: Store<FavoritesState>, private loaderService: LoaderService) { }

  loadFavorite(): void {
    this.loaderService.startLoading(4);

    this.loadFavoriteChannelIds()
      .then(data => this.favoriteStore.dispatch(new LoadFavoriteChannels(data.channels)))
      .finally(() => this.loaderService.loadFinished());

    this.http.get<FavoriteVideos>(this.BASE_URL.concat('v2/favorite/videos')).toPromise()
      .then(data => this.favoriteStore.dispatch(new LoadFavoriteVideos(data.videos)))
      .finally(() => this.loaderService.loadFinished());

    this.http.get<FavoriteTvshows>(this.BASE_URL.concat('v2/favorite/tvshows')).toPromise()
      .then(data => this.favoriteStore.dispatch(new LoadFavoriteTvshows(data.tvshows)))
      .finally(() => this.loaderService.loadFinished());

    this.http.get<FavoriteBooks>(this.BASE_URL.concat('v2/favorite/litres-items')).toPromise()
      .then(data => this.favoriteStore.dispatch(new LoadFavoriteBooks(data.litres)))
      .finally(() => this.loaderService.loadFinished());
  }

  getFavoriteChannelIds(): FavoriteChannel[] {
    let state: FavoriteChannel[];
    this.favoriteStore.select('favoritesData').pipe(map(data => data.channels)).subscribe(s => state = s);
    return state;
}

  loadFavoriteChannelIds(): Promise<FavoriteChannels> {
    return this.http.get<FavoriteChannels>(this.BASE_URL.concat('v2/favorite/channels')).toPromise();
  }

  isChannelFavorite(id: number): Observable<boolean> {
    return this.favoriteStore.select('favoritesData').pipe(map(data => {
      return data.channels.some(channel => channel.channel_id === id);
    }));
  }

  isTvshowFavorite(id: string): Observable<boolean> {
    return this.favoriteStore.select('favoritesData').pipe(map(data => {
      return data.tvshows.some(tvshow => tvshow.tvshow_id.toString() === id);
    }));
  }

  isVideoFavorite(id: number): Observable<boolean> {
    return this.favoriteStore.select('favoritesData').pipe(map(data => {
      return data.videos.some(video => video.video_id === id);
    }));
  }

  isBookFavorite(id: number): Observable<boolean> {
    return this.favoriteStore.select('favoritesData').pipe(map(data => {
      return data.litres.some(book => book.litres_item_id === id);
    }));
  }

  removeFromFavorite(id: any, type: ContentType): Promise<any> {
    return new Promise((resolve, reject) => {
      const params: HttpParams = new HttpParams().set('id', id.toString());
      this.loaderService.startLoading(1);
      this.http.delete(this.BASE_URL.concat('v2/favorite/', this.getRoute(type)), { params }).toPromise().then(() => {
        switch (type) {
          case ContentName.CHANNEL:
            this.favoriteStore.dispatch(new DeleteFavoriteChannel(+id));
            break;

          case ContentName.TV:
            this.favoriteStore.dispatch(new DeleteFavoriteTvshow(id.toString()));
            break;

          case ContentName.VIDEO:
            this.favoriteStore.dispatch(new DeleteFavoriteVideo(+id));
            break;

          case ContentName.BOOK:
            this.favoriteStore.dispatch(new DeleteFavoriteBook(+id));
            break;

          default:
            break;
        }

        resolve();
      }).catch(_ => reject())
      .finally(() => this.loaderService.loadFinished());
    });
  }

  addToFavorite(id: any, type: ContentType, timestamp: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loaderService.startLoading(1);
      const params: HttpParams = new HttpParams().set('id', id.toString());
      this.http.post(this.BASE_URL.concat('v2/favorite/', this.getRoute(type)), {}, { params }).toPromise().then(() => {
        switch (type) {
          case ContentName.CHANNEL:
            const ch: FavoriteChannel = {
              channel_id: +id,
              added_time: timestamp
            };
            this.favoriteStore.dispatch(new AddFavoriteChannel(ch));
            break;

          case ContentName.TV:
            const tv: FavoriteTvshow = {
              tvshow_id: id.toString(),
              added_time: timestamp
            };
            this.favoriteStore.dispatch(new AddFavoriteTvshow(tv));
            break;

          case ContentName.VIDEO:
            const video: FavoriteVideo = {
              video_id: +id,
              added_time: timestamp
            };
            this.favoriteStore.dispatch(new AddFavoriteVideo(video));
            break;

          case ContentName.BOOK:
            const book: FavoriteBook = {
              litres_item_id: +id,
              added_time: timestamp
            };
            this.favoriteStore.dispatch(new AddFavoriteBook(book));
            break;

          default:
            break;
        }
        resolve();
      }).catch(_ => reject())
      .finally(() => this.loaderService.loadFinished());
    });
  }

  resetFavorite(): void {
    this.favoriteStore.dispatch(new LoadFavoriteChannels([]));
    this.favoriteStore.dispatch(new LoadFavoriteVideos([]));
    this.favoriteStore.dispatch(new LoadFavoriteTvshows([]));
    this.favoriteStore.dispatch(new LoadFavoriteBooks([]));
  }

  private getRoute(type: string): string {
    switch (type) {
      case ContentName.TV:
        return 'tvshow';
      case ContentName.BOOK:
        return 'litres-item';
      default:
        return type;
    }
  }
}

interface FavoriteChannels {
  channels: FavoriteChannel[];
}

interface FavoriteVideos {
  videos: FavoriteVideo[];
}

interface FavoriteTvshows {
  tvshows: FavoriteTvshow[];
}

interface FavoriteBooks {
  litres: FavoriteBook[];
}
