import { DataService, AuthService, FavoriteService, LoaderService } from '@services/core';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserInfo } from '@models/core';

@Component({
  selector: 'app-info-page',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})

export class InfoPageComponent implements OnInit {
  public userInfo: UserInfo;

  constructor(
    private authService: AuthService,
    private router: Router,
    private dataService: DataService,
    private favoriteService: FavoriteService,
    private loaderService: LoaderService
  ) {}


  ngOnInit() {
    this.loaderService.startLoading(1);
    this.authService.getAccountInfo().then(res => {
      this.userInfo = res;
      this.loaderService.loadFinished();
    });
  }

  public logOut(): void {
    this.authService.logout();
    this.dataService.loadChannels();
    this.favoriteService.resetFavorite();
    this.router.navigate(['home']);
  }

}
