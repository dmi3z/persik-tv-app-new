import { NavModule } from './../../directives/nav.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountPageComponent } from './account.component';
import { RouterModule } from '@angular/router';
import { InfoPageComponent } from './info/info.component';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  declarations: [
    AccountPageComponent,
    InfoPageComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    NavModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccountPageComponent,
        children: [
          {
            path: '',
            redirectTo: 'main',
            pathMatch: 'full'
          },
          {
            path: 'main',
            component: InfoPageComponent
          },
          {
            path: 'subscriptions',
            loadChildren: './subscriptions/subscriptions.module#SubscriptionsPageModule'
          },
          {
            path: 'payments',
            loadChildren: './payments/payments.module#PaymentsPageModule'
          },
          {
            path: 'course-pay',
            loadChildren: './course-pay/course-pay.module#CoursePayPageModule'
          },
          {
            path: 'book-pay',
            loadChildren: './book-pay/book-pay.module#BookPayPageModule'
          }
        ]
      }
    ])
  ]
})

export class AccountPageModule {}
