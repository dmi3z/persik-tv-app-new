import { LoaderService } from './../../../services/loader.service';
import { AuthService } from '@services/core';
import { Component, OnInit } from '@angular/core';
import { UserSubscription } from '@models/core';

@Component({
  selector: 'app-subscription-page',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss']
})

export class SubscriptionsPageComponent implements OnInit {

  public subscriptions: UserSubscription[] = [];

  constructor(private authService: AuthService, private loaderService: LoaderService) {}

  ngOnInit() {
    this.loaderService.startLoading(1);
    this.loadUserSubscriptions();
  }

  public get isHaveSubscriptions(): boolean {
    return this.subscriptions.length > 0;
  }

  private loadUserSubscriptions(): void {
    this.authService.getUserSubscriptions().then(res => {
      this.subscriptions = res;
    }).finally(() => this.loaderService.loadFinished());
  }

}
