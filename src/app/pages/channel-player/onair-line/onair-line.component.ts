import { Tvshow } from '@models/core';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-onair-line',
  templateUrl: 'onair-line.component.html',
  styleUrls: ['onair-line.component.scss'],
})

export class OnairLineComponent {

  @Input() tvshow: Tvshow;
  @Input() currentTime: number;

  constructor() {}

  get progress(): number {
    if (this.tvshow) {
      return Math.round(((this.currentTime - this.tvshow.start) / (+this.tvshow.stop - this.tvshow.start)) * 1000) / 10;
    }
    return 0;
  }
}
