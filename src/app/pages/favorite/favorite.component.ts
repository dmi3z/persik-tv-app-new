import { FavoriteService } from './../../services/favorite.service';
import { Book } from './../audiobooks/litres.model';
import { Router } from '@angular/router';
import { BackService } from '@services/core';
import { Channel } from './../../models/channel';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { FavoritesState } from 'src/redux/favorite/favorite.state';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { FavoritesData, FavoriteBook, FavoriteVideo, FavoriteTvshow } from '@models/core';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { LitresService } from '../audiobooks/litres.service';

@Component({
  selector: 'app-favorite-page',
  templateUrl: 'favorite.component.html',
  styleUrls: ['favorite.component.scss']
})

export class FavoritePageComponent implements OnInit, OnDestroy {

  private backServiceSubscriber: Subscription;

  public favoriteChannels: Observable<Channel[]>;
  public favoriteVideoIds: Observable<FavoriteVideo[]>;
  public favoriteBookIds: Observable<FavoriteBook[]>;
  public boughtBooks: Observable<Book[]>;
  public favoriteTvshowIds: Observable<FavoriteTvshow[]>;

  constructor(
    private favoriteStore: Store<FavoritesState>,
    private channelStore: Store<ChannelsState>,
    private router: Router,
    private backService: BackService,
    private litresService: LitresService,
    private favoriteService: FavoriteService
  ) { }

  ngOnInit() {
    this.backServiceSubscriber = this.backService.backEvent.subscribe(_ => this.backService.goToMain());
    this.loadFavoriteChannels();
    this.loadFavoriteBooks();
    this.loadBoughtBooks();
    this.loadFavoriteVideos();
    this.loadFavoriteTvshows();
    this.focusOnFirst();
  }

  public backToMain(): void {
    this.router.navigate(['home']);
  }

  private get favorites(): Observable<FavoritesData> {
    return this.favoriteStore.select('favoritesData');
  }

  private get channels(): Observable<Channel[]> {
    return this.channelStore.select('channelsData').pipe(map(data => data.channels));
  }

  private loadFavoriteChannels(): void {
    const favoriteChannelIds = this.favoriteService.getFavoriteChannelIds().map(item => item.channel_id);
    this.favoriteChannels = this.channels.pipe(map(data => data.filter(channel => favoriteChannelIds.includes(channel.channel_id))));
  }

  private loadFavoriteBooks(): void {
    this.favoriteBookIds = this.favorites.pipe(map(data => data.litres));
  }

  private loadBoughtBooks(): void {
    this.boughtBooks = this.litresService.getUserBooks();
  }

  private loadFavoriteVideos(): void {
    this.favoriteVideoIds = this.favorites.pipe(map(data => data.videos));
  }

  private loadFavoriteTvshows(): void {
    this.favoriteTvshowIds = this.favorites.pipe(map(data => data.tvshows));
  }

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 1000);
  }

  ngOnDestroy() {
    if (this.backServiceSubscriber) {
      this.backServiceSubscriber.unsubscribe();
    }
  }

}
