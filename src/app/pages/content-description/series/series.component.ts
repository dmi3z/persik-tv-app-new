import { LoaderService } from '@services/core';
import { DataService } from '@services/core';
import { Video, Episode } from '@models/core';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss']
})

export class SeriesComponent {

  @Input() video: Video;
  public activeSeason: string;
  public seriesVideoInformation: Video[] = [];
  @Output() playEvent = new EventEmitter<Video>();

  constructor(private dataService: DataService, private loaderService: LoaderService) { }

  public get seasons(): string[] { // Получение массива уникальных названий сезонов
    return this.video.episodes.map(item => item.season).filter((item, index, self) => self.indexOf(item) === index); // .sort()
  }

  public isActiveSeason(season: string): boolean {
    return season === this.activeSeason;
  }

  public loadSeriesBySeason(season: string): void { // Получение серий по номеру сезона
    this.loaderService.startLoading(1);
    if (this.activeSeason !== season) { // предотвращаем повторную загрузку того же контента
      this.activeSeason = season;
      this.seriesVideoInformation = [];
      const series = this.video.episodes.filter(item => item.season === this.activeSeason).sort((a, b) => {
        return +a.episode.split(' ')[1] - +b.episode.split(' ')[1];
      });
      this.loadInfo(series);
    } else {
      this.activeSeason = null;
      this.loaderService.loadFinished();
    }
  }

  public playVideo(video: Video): void {
    this.playEvent.emit(video);
  }

  private loadInfo(series: Episode[]): void {
    const serieIds: any[] = series.map(s => {
      return s.tvshow_id ? s.tvshow_id.toString() : s.video_id;
    });
    this.dataService.getVideosInfo(serieIds).then(info => {
      this.seriesVideoInformation = info.map(item => {
        const name = series.find(sr => {
          const id = sr.tvshow_id ? sr.tvshow_id : sr.video_id;
          const itemId = item.tvshow_id ? item.tvshow_id : item.video_id;
          return id === itemId;
        }).episode;
        const video = item;
        video.name = name;
        return video;
      });
    }).finally(() => this.loaderService.loadFinished());
  }

}
