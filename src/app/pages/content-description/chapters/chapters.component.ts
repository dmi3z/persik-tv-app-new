import { LitresService } from './../../audiobooks/litres.service';
import { Book } from './../../audiobooks/litres.model';
import { ContentName } from '@models/core';
import { Component, Input } from '@angular/core';
import { BookFile } from '../../audiobooks/litres.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chapters',
  templateUrl: 'chapters.component.html',
  styleUrls: ['chapters.component.scss']
})

export class ChaptersComponent {
  @Input() book: Book;

  public isOpened: boolean;

  constructor(private router: Router, private litresService: LitresService) { }

  public playChapter(chapter: BookFile): void {
    this.router.navigate(['video-player', ContentName.BOOK, this.book.id],
      {
        queryParams: {
          cover: this.litresService.getCover(this.book.id, this.book.cover),
          stream: chapter.stream
        }
      });
  }

  public toggleChapters(): void {
    this.isOpened = !this.isOpened;
  }

  public get chapters(): BookFile[] {
    return this.book.file_groups[1].files;
  }
}
