import { map } from 'rxjs/operators';
import { Book, BookFile } from './../audiobooks/litres.model';
import { ContentName, Channel, Person } from '@models/core';
import { Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Tvshow, ContentType, Video } from '@models/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService, BackService, TimeService, AuthService, FavoriteService, LoaderService } from '@services/core';
import { Location } from '@angular/common';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { LitresService } from '../audiobooks/litres.service';

@Component({
  selector: 'app-content-description',
  templateUrl: './content-description.component.html',
  styleUrls: ['./content-description.component.scss']
})

export class ContentDescriptionPageComponent implements OnInit, OnDestroy {

  private backServiceSubscriber: Subscription;
  private channelStoreSubscriber: Subscription;

  private type: ContentType;
  public video: Video;
  public tvshow: Tvshow;
  public genres: string[];
  public authors: string[];
  public directors: Person[];
  public casts: Person[];
  public channel: Channel;
  public isCanPlay: boolean;
  public bookInfo: Book;
  public isBought: Observable<boolean>;
  public chapters: BookFile[] = [];
  public contentName: string;

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private backService: BackService,
    private location: Location,
    private channelStore: Store<ChannelsState>,
    private timeService: TimeService,
    private authService: AuthService,
    private router: Router,
    private favoriteService: FavoriteService,
    private litresService: LitresService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.backServiceSubscriber = this.backService.backEvent.subscribe(_ => {
      this.location.back();
    });
    const params = this.activatedRoute.snapshot.params;
    this.type = params.type;
    if (this.type !== ContentName.BOOK) {
      this.video = this.dataService.activeVideo;
      this.tvshow = this.dataService.activeTvshow;
      this.genres = this.dataService.currentGenres;
      this.detectIsCanPlay();
      this.loaderService.startLoading(2);
      this.loadDitectors();
      this.loadCasts();
      this.loadChannelInfo();
      this.contentName = this.getNameWithColor(this.video.name);
    } else {
      this.bookInfo = this.dataService.activeAudiobook;
      this.contentName = this.getNameWithColor(this.bookInfo.title);
      this.loadIsBought();
      this.genres = this.bookInfo.genres.map(genre => genre.title);
      this.casts = this.bookInfo.authors.map(author => {
        const person: Person = {
          info: null,
          international_name: null,
          person_id: null,
          photo: null,
          name: author['first-name'] + ' ' + author['last-name']
        };
        return person;
      });
      this.video = {
        age_rating: this.bookInfo.adult.toString(),
        category_id: 7,
        cast: [],
        art: null,
        cover: this.litresService.getCover(this.bookInfo.id, this.bookInfo.cover),
        duration: this.bookInfo.chars,
        description: this.bookInfo.annotation,
        name: this.bookInfo.title,
        genres: [],
        episodes: [],
        is_pladform: false,
        is_series: this.bookInfo.file_groups && this.bookInfo.file_groups[1] && this.bookInfo.file_groups[1].files && this.bookInfo.file_groups[1].files.length > 1 ? true : false,
        director: [],
        countries: [],
        international_name: '',
        ratings: null,
        video_id: this.bookInfo.id,
        year: this.bookInfo.date_written_s ? this.bookInfo.date_written_s.toString() : null
      };
      this.chapters = this.bookInfo.file_groups ? this.bookInfo.file_groups[1].files : [];
      this.genres = this.bookInfo.genres.map(genre => genre.title);
      this.authors = this.bookInfo.authors.map(author => {
        return author['first-name'] + ' ' + author['last-name'];
      });
    }
    this.focusOnFirst();
  }

  private getNameWithColor(name: string): string {
    if (name) {
      const nameArray: string[] = name.split(' ');
      if (nameArray.length === 1) {
        return name;
      }
      let colorString: string = nameArray.shift();
      colorString += ' <span class="orange-text">';
      const lastText: string = nameArray.toString();
      colorString += lastText.replace(new RegExp(',', 'g'), ' ');
      colorString += '</span>';
      return colorString;
    }
  }

  public loadIsBought(): void {
    if (this.isAuth) {
      this.isBought = this.litresService.getUserBooks().pipe(map(data => {
        this.focusOnFirst();
        if (data) {
          return !!data.find(book => book.id === this.bookInfo.id);
        }
        return false;
      }));
    }
  }

  public get isAudiobook(): boolean {
    return this.type === ContentName.BOOK;
  }

  public get isAuth(): boolean {
    return this.authService.isLogin;
  }

  public get isSeries(): boolean {
    return this.video.is_series;
  }

  public buyBook(): void {
    this.router.navigate(['account/book-pay']);
  }

  public playVideo(video?: Video): void {
    if (video) {
      const type = video.tvshow_id ? ContentName.TV : ContentName.VIDEO;
      const id = video.tvshow_id ? video.tvshow_id : video.video_id;
      this.router.navigate(['video-player', type, id]);
    } else {
      if (this.video.is_series && this.video.episodes.length > 0) {
        const firstEpisode = this.video.episodes[0];
        const firstIdToPlay = firstEpisode.type === 'tvshow' ? firstEpisode.tvshow_id : firstEpisode.video_id;
        const type = firstEpisode.type === 'tvshow' ? ContentName.TV : ContentName.VIDEO;
        this.router.navigate(['video-player', type, firstIdToPlay]);
      } else {
        if (this.tvshow) {
          if (this.tvshow.tvshow_id) {
            this.router.navigate(['video-player', ContentName.TV, this.tvshow.tvshow_id]);
            return;
          }
          if (this.tvshow.video_id) {
            this.router.navigate(['video-player', ContentName.VIDEO, this.tvshow.video_id]);
            return;
          }
        } else {
          this.router.navigate(['video-player', ContentName.VIDEO, this.video.video_id]);
        }
      }
    }


    // if (this.isSeries) {
    //   let id;
    //   let type;
    //   if (video) {
    //     id = video.tvshow_id ? video.tvshow_id : video.video_id;
    //   } else {
    //     const videoId = this.video.episodes[0] && this.video.episodes[0].video_id ? this.video.episodes[0].video_id : null;
    //     const tvshowId = this.video.episodes[0] && this.video.episodes[0].tvshow_id ? this.video.episodes[0].tvshow_id : null;
    //     if (videoId || tvshowId) {
    //       id = videoId ? videoId : tvshowId;
    //     } else {
    //       id = this.video.tvshow_id ? this.video.tvshow_id : this.video.video_id;
    //     }
    //     type = videoId ? ContentName.VIDEO : ContentName.TV;
    //   }
    //   // this.router.navigate(['video-player', type, id]);
    // } else {
    //   const id = this.type === ContentName.TV ? this.tvshow.tvshow_id : this.video.video_id;
    //   this.router.navigate(['video-player', this.type, id]);
    // }
  }

  public playTrial(): void {
    setTimeout(() => {
      this.router.navigate(['video-player', ContentName.BOOK, this.video.video_id],
      {
        queryParams: {
          cover: this.video.cover,
          stream: this.litresService.getTrial(this.video.video_id)
        }
      });
    }, 100);
  }

  playFull(): void {
    const stream = this.bookInfo.file_groups[1].files[0].stream;
    this.router.navigate(['video-player', ContentName.BOOK, this.video.video_id],
      {
        queryParams: {
          cover: this.video.cover,
          stream
        }
      });
  }

  public authorization(): void {
    this.router.navigate(['auth']);
  }

  public scrollToTop(): void {
    const page = document.querySelector('.page');
    if (page) {
      try {
        page.scrollTo(0, 0);
      } catch (e) {
        console.log('Scroll top is not a function');
      }
    }
  }

  public get isFavorite(): Observable<boolean> {
    switch (this.type) {
      case ContentName.TV:
        return this.favoriteService.isTvshowFavorite(this.tvshow.tvshow_id);

      case ContentName.BOOK:
        return this.video && this.favoriteService.isBookFavorite(this.video.video_id);

      default:
        return this.favoriteService.isVideoFavorite(this.video.video_id);
    }

  }

  public addToFavorite(): void {
    this.loaderService.startLoading(1);
    const id = (this.tvshow && this.tvshow.tvshow_id) ? this.tvshow.tvshow_id : this.video.video_id;
    this.favoriteService.addToFavorite(id, this.type, this.timeService.currentTime)
      .then(_ => this.focusOnFirst())
      .finally(() => this.loaderService.loadFinished());
  }

  public removeFromFavorite(): void {
    this.loaderService.startLoading(1);
    const id = (this.tvshow && this.tvshow.tvshow_id) ? this.tvshow.tvshow_id : this.video.video_id;
    this.favoriteService.removeFromFavorite(id, this.type)
      .then(_ => this.focusOnFirst())
      .finally(() => this.loaderService.loadFinished());
  }

  private detectIsCanPlay(): void {
    if (this.type === ContentName.VIDEO) {
      this.isCanPlay = true;
    }
  }

  private loadDitectors(): void {
    if (this.video.director && this.video.director.length > 0) {
      this.dataService.loadActors(this.video.director)
        .then(data => this.directors = data)
        .catch(_ => console.log('Bad request'))
        .finally(() => this.loaderService.loadFinished());
    } else {
      this.loaderService.loadFinished();
    }
  }

  private loadCasts(): void {
    if (this.video.cast && this.video.cast.length > 0) {
      this.dataService.loadActors(this.video.cast)
        .then(data => this.casts = data)
        .catch(_ => console.log('Bad request'))
        .finally(() => this.loaderService.loadFinished());
    } else {
      this.loaderService.loadFinished();
    }
  }

  private loadChannelInfo(): void {
    if (this.tvshow) {
      this.channelStoreSubscriber = this.channelStore.select('channelsData').subscribe(res => {
        this.channel = res.channels.find(ch => ch.channel_id === this.tvshow.channel_id);
        const currentTime = this.timeService.currentTime;
        const dvr = currentTime - this.channel.dvr_sec;
        if (dvr < this.tvshow.start && this.tvshow.stop < currentTime && this.channel.available) {
          this.isCanPlay = true;
        } else {
          this.isCanPlay = false;
        }
      });
    }
  }


  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 300);
  }

  ngOnDestroy() {
    if (this.backServiceSubscriber) {
      this.backServiceSubscriber.unsubscribe();
    }
    if (this.channelStoreSubscriber) {
      this.channelStoreSubscriber.unsubscribe();
    }
  }
}
