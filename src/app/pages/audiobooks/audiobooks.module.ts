import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NavModule } from './../../directives/nav.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AudiobooksPageComponent } from './audiobooks.component';
import { RowMatrixPipe } from './row-matrix.pipe';
import { AudiobookCardModule } from 'src/app/components/audiobook-card/audiobook-card.module';

@NgModule({
  declarations: [
    AudiobooksPageComponent,
    RowMatrixPipe
  ],
  imports: [
    CommonModule,
    InfiniteScrollModule,
    AudiobookCardModule,
    NavModule,
    RouterModule.forChild([
      {
        path: '',
        component: AudiobooksPageComponent
      }
    ])
  ]
})

export class AudiobooksPageModule { }
