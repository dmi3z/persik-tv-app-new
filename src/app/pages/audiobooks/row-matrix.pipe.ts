import { Pipe, PipeTransform } from '@angular/core';
import { Book } from './litres.model';

@Pipe({
  name: 'rowMatrixPipe'
})

export class RowMatrixPipe implements PipeTransform {
  transform(books: Book[]): Array<Book[]> {
    const booksMatrix: Array<Book[]> = [];
    let row: Book[] = [];
    books.forEach((book, index) => {
      row.push(book);
      if ((index + 1) % 4 === 0 || index === books.length - 1) {
        booksMatrix.push(row);
        row = [];
      }
    });
    return booksMatrix;
  }
}
