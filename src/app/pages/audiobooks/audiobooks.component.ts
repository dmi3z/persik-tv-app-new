import { Genre } from '@models/core';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { LitresService } from './litres.service';
import { Book } from './litres.model';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '@services/core';

@Component({
  selector: 'app-audiobooks-page',
  templateUrl: 'audiobooks.component.html',
  styleUrls: ['audiobooks.component.scss']
})

export class AudiobooksPageComponent implements OnInit {

  public genres: Observable<Genre[]>;
  public activeGenre: Genre;
  public startGenre: Observable<Genre>;
  public books: Array<Book[]> = [];
  private activatedRouteSubscriber: Subscription;

  private skip = 0;
  private step = 20;

  constructor(private litresService: LitresService, private activatedRoute: ActivatedRoute, private loaderService: LoaderService) { }

  ngOnInit() {
    // this.loadStartGenre();
    // this.loadGenres();
    this.activatedRouteSubscriber = this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.genre) {
        this.activeGenre = JSON.parse(params.genre);
      } else {
        this.activeGenre = {
          id: 0,
          is_main: true,
          name: 'Все жанры',
          name_en: ''
        };
      }
      this.onGenreChange();
    });
  }

  public onGenreChange(): void {
    this.skip = 0;
    this.books = [];
    this.loadBooks();
  }

  public loadBooks(): void {
    this.loaderService.startLoading(1);
    this.litresService.getListPromise(this.activeGenre.id, this.skip, this.step).then(response => {
      let row: Book[] = [];
      if (response && response.length) {
        for (let i = 0; i < response.length; i++) {
          if (i % 4 === 0 && i !== 0) {
            this.books.push( row );
            row = [];
          }
          row.push(response[i]);
          if (this.books.length === 1 && this.books[0].length > 0) {
            this.focusOnFirst();
          }
          if (i === response.length - 1) {
            this.books.push( row );
          }
        }
        // this.stopLoading();
        this.skip += this.step;
      }
    }).finally(() => this.loaderService.loadFinished());
  }


  /* private loadStartGenre(): void {
    this.startGenre = this.litresService.getGenres().pipe(map(genres => {
      this.activeGenre = genres[0];
      this.loadBooks();
      return genres[0];
    }));
  } */

  /* private loadGenres(): void {
    this.genres = this.litresService.getGenres();
  } */

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 300);
  }
}
