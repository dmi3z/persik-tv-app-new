import { Genre } from '@models/core';
import { GenreFilterComponent } from './genre-filter.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

describe('GenreFilterComponent', () => {
  let component: GenreFilterComponent;
  let fixture: ComponentFixture<GenreFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenreFilterComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change activeGenre', () => {
    const genre: Genre = {
      id: 0,
      is_main: true,
      ch_count: 10,
      name: 'Test',
      name_en: 'test'
    };
    component.selectGenre(genre);
    expect(component.activeGenre).toEqual(genre);
  });

});
