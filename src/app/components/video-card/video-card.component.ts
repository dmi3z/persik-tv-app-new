import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ImageService, DataService } from '@services/core';
import { Video, ContentId, ContentName, Genre, Tvshow } from '@models/core';
import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { VodState } from '../../../redux/vod/vod.state';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss']
})

export class VideoCardComponent implements OnInit, OnDestroy {

  @Input() id: ContentId;
  @Input() type = 'video';
  @Input() countInRow: number;
  @Input() isLast: boolean;
  @Input() isStub: boolean;
  @Output() showAllEvent = new EventEmitter<any>();
  public video: Video;
  public tvshow: Tvshow;
  public videoGenres: string[] = [];

  private loadVodSubscriber: Subscription;
  private isCanRedirect: boolean;

  constructor(
    private dataService: DataService,
    private imageService: ImageService,
    private vodStore: Store<VodState>,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.isVideo && !this.isLast) {
      this.loadVideoInfo(this.id.video_id);
    } else {
      this.loadTvshowInfo();
    }
  }

  public get isFiveInRow(): boolean {
    return this.countInRow === 5;
  }

  public get isVideo(): boolean {
    if (!this.isLast && !this.isStub && this.id.video_id) {
      return true;
    }
    return false;
  }

  public get isVideoThumbnail(): boolean {
    return this.type === 'video';
  }

  public showAll(): void {
    this.showAllEvent.emit();
  }

  private loadGenres(): void { // Получение списка жанров для видео по id жанров
    this.loadVodSubscriber = this.vodStore.select('vodCategories').subscribe(data => {
      const allGenresArray = data.categories.map(cat => cat.genres);
      const allGenres: Genre[] = [];
      allGenresArray.forEach(genre => {
        allGenres.push(...genre);
      });

      this.videoGenres = allGenres.filter(genre => {
        return this.video.genres.some(genreId => genreId === genre.id);
      }).map(res => {
        const name = res.name;
        const part = name.split(')');
        if (part[1]) {
          return part[1];
        }
        return name;
      });
    });
  }

  public openDescription(): void {
    this.dataService.activeTvshow = null;
    this.dataService.activeVideo = null;
    if (this.tvshow) {
      this.dataService.activeTvshow = this.tvshow;
    }
    this.dataService.activeVideo = this.video;
    this.dataService.currentGenres = this.videoGenres;
    const type = this.isVideo ? ContentName.VIDEO : ContentName.TV;
    if (this.isCanRedirect) {
      this.router.navigate(['content-description', type]);
    }
  }

  private loadVideoInfo(id: number): void {
    this.dataService.getVideoInfo(id).then(res => {
      this.video = res;
      if (this.video.cover === null && this.tvshow) {
        const cover = this.imageService.getTvshowFrame(this.tvshow);
        this.video.cover = cover;
      }
      this.isCanRedirect = true;
      this.loadGenres();
    });
  }

  private loadTvshowInfo(): void {
    if (!this.isLast && !this.isStub) {
      this.dataService.getTvshowInfo(this.id.tvshow_id).then(res => {
        this.tvshow = res;
        this.loadVideoInfo(res.video_id);
      });
    }
  }

  ngOnDestroy() {
    if (this.loadVodSubscriber) {
      this.loadVodSubscriber.unsubscribe();
    }
  }
}
