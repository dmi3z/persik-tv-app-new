import { CommonModule } from '@angular/common';
import { ChannelCardComponent } from './channel-card.component';
import { NgModule } from '@angular/core';
import { NavModule } from '../../directives/nav.module';
import { TitlePipe } from './title.pipe';
import { ProgressPipe } from './progress.pipe';
@NgModule({
  declarations: [
    ChannelCardComponent,
    TitlePipe,
    ProgressPipe
  ],
  imports: [
    CommonModule,
    NavModule
  ],
  exports: [
    ChannelCardComponent
  ]
})

export class ChannelCardModule {}
