import { Component, Input, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Channel } from '@models/core';
import { TimeService, DataService } from '@services/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-channels-tape',
  templateUrl: 'channels-tape.component.html',
  styleUrls: ['channels-tape.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelsTapeComponent implements OnInit {

  @Input() channels: Channel[];
  @Input() stub: boolean;
  @Input() title: string;
  public currentTime: number;

  constructor(
    private router: Router,
    private timeService: TimeService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.currentTime = this.timeService.currentTime;
    /* this.timerSubscriber = this.timeService.timeController.subscribe(time => {
      this.currentTime = time;
      // this.checkFinishedTvshows();
    }); */
  }

  /* private createChannelTape(): void {
    const ids = this.channels.map(ch => ch.channel_id);
    this.dataService.getCurrentTvShows(ids).then(res => {
      this.channelTape = this.channels.map(item => {
        const elem: ChannelCard = {
          channel: item,
          tvshow: res.find(t => +t.channel_id === +item.channel_id)
        };
        return elem;
      });
      // this.cdr.detectChanges();
    });
  } */

  /* private checkFinishedTvshows(): void {
    this.cdr.markForCheck();
    if (this.channelTape && this.channelTape.length > 0) {
      this.channelTape.forEach(tape => {
        if (tape && tape.tvshow && tape.tvshow.stop && +tape.tvshow.stop < this.currentTime) {
          this.dataService.getCurrentTvShows([tape.channel.channel_id]).then(result => {
            tape.tvshow = result[0];
            this.cdr.detectChanges();
          });
        }
      });
    }
  } */

  /* public get isHaveChannels(): boolean {
    return this.tape.channels.length > 0;
  } */

  /* public get isHaveTitle(): boolean {
    return (this.tape.title && this.tape.title.length > 0);
  } */

  public onShowAll(): void {
    this.dataService.activeGenreId = 0;
    this.router.navigate(['tv-review']);
  }
}
