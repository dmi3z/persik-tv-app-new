import { Subscription } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { ContentType, ContentName, Channel, PlayerEvents } from '@models/core';
import { Component, ViewChild, ElementRef, OnInit, OnDestroy, Renderer2, ChangeDetectionStrategy } from '@angular/core';
import * as Hls from 'hls.js';
import * as NowPlaying from '../../../../platforms/android/plugins/cordova-plugin-nowplaying/www/NowPlaying.js';
import {
  AuthService,
  DataService,
  ImageService,
  TimeService,
  AdultService,
  PlayerControllerService,
  LoaderService
} from '@services/core';

import { Store } from '@ngrx/store';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { AdultCheckState } from 'src/app/services/adult.service';


@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class PlayerComponent implements OnInit, OnDestroy {
  @ViewChild('video') video: ElementRef;
  @ViewChild('player') playerElem: ElementRef;

  public isPlayerHaveError: boolean;
  public contentPoster: string;

  private hls: any;
  private player: HTMLVideoElement;
  private videoId: number;
  private channels: Channel[] = [];

  private channelSubscriber: Subscription;
  private playerEventSubscriber: Subscription;

  private stateChangeHandler: any;
  private playHandler: any;
  private pauseHandler: any;
  private currentChannelId: number;

  private isListenersIsSet: boolean;

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private imageService: ImageService,
    private timeService: TimeService,
    private channelStore: Store<ChannelsState>,
    private adultService: AdultService,
    private playerController: PlayerControllerService,
    private renderer: Renderer2,
    private loaderService: LoaderService
    // private litresService: LitresService
    // private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.player = this.video.nativeElement;

    this.channelSubscriber = this.channelStore.select('channelsData').subscribe(data => {
      this.channels = data.channels;
    });

    this.stateChangeHandler = this.onPlayerStateChange.bind(this);
    this.playHandler = this.onPlayerPlay.bind(this);
    this.pauseHandler = this.onPlayerPause.bind(this);

    this.playerController.actions.subscribe(event => {
      switch (event.name) {
        case 'play':
          this.play(event.id, event.type, event.cover, event.stream);
          break;

        case 'pause':
          this.pause();
          break;

        case 'stop':
          this.stop();
          break;

        case 'updateChannelId':
          this.playChannel(event.id);
          break;

        case 'seek':
          this.seek(event.value);
          break;

        case 'resume':
          this.resume();
          break;

        case 'setFullscreen':
          this.setFullscreenMode();
          break;

        case 'setThumbnail':
          this.setThumbnailMode();
          break;

        default:
          break;
      }
    });
  }

  public get screenWidth(): string {
    const width = window.innerWidth;
    return width ? width + 'px' : '100%';
  }

  public get screenHeight(): string {
    const height = window.innerHeight;
    return height ? height + 'px' : '100%';
  }

  public get logo(): string {
    if (this.currentChannelId) {
      return this.imageService.getChannelLogo(this.currentChannelId);
    }
    return null;
  }

  private play(id: number | string, type: ContentType, cover?: string, stream?: string) {
    if (id.toString().length > 15) {
      type = ContentName.TV;
    }
    if (this.isAuthorized || type === ContentName.BOOK) {
      switch (type) {
        case ContentName.CHANNEL:
          if (this.currentChannelId !== +id) {
            this.currentChannelId = +id;
            this.playChannel(+id);
          }
          break;

        case ContentName.VIDEO:
          this.videoId = +id;
          this.playVideo(+id);
          break;

        case ContentName.TV:
          this.playTvshow(id.toString());
          break;

        case ContentName.BOOK:
          this.playAudio(stream, cover);
          break;

        default:
          break;
      }
    } else {
      setTimeout(() => {
        this.currentChannelId = +id;
        this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime, 'scale', 1024, 768);
        this.isPlayerHaveError = true;
        this.playerController.events.next(PlayerEvents.PLAYER_ERROR_LOGIN);
      }, 0);
    }
  }

  private stop(clearPoster = true) {
    this.player.pause();
    if (this.hls) {
      this.hls.stopLoad();
      this.hls.detachMedia();
      this.destroyHls();
    }
    if (this.player) {
      this.player.removeAttribute('src');
    }
    if (clearPoster) {
      // this.contentPoster = null;
    }
    this.currentChannelId = null;
  }

  private seek(value: number) {
    this.player.currentTime = value;
  }

  private resume(): void {
    this.player.play();
  }

  private pause(): void {
    this.playerController.events.next(PlayerEvents.PLAYER_PAUSE);
    if (this.player) {
      this.player.pause();
    }
  }

  private setFullscreenMode() {
    this.renderer.removeClass(this.playerElem.nativeElement, 'player_thumbnail');
    this.renderer.addClass(this.playerElem.nativeElement, 'player_fullscreen');
    this.renderer.removeClass(this.video.nativeElement, 'player-video_thumbnail');
  }

  private setThumbnailMode() {
    this.renderer.addClass(this.playerElem.nativeElement, 'player_thumbnail');
    this.renderer.removeClass(this.playerElem.nativeElement, 'player_fullscreen');
    this.renderer.addClass(this.video.nativeElement, 'player-video_thumbnail');
  }

  private destroyHls() {
    this.hls.destroy();
    this.hls = null;
  }

  private playAudio(stream: string, cover: string) {
    this.contentPoster = cover;
    this.playWithStandartHtml(stream);
  }

  private playChannel(id: number): void {
    this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime, 'scale', 1024, 768);
    this.loaderService.startLoading(1);
    this.dataService.getChannelStream(id).then(res => {
      this.loaderService.loadFinished();
      const channel = this.channels.find(ch => ch.channel_id === id);
      if (channel.age_rating.includes('18')) {
        this.playerController.events.next(PlayerEvents.PLAYER_ADULT_CONTENT);
        this.stop();
        if (this.playerEventSubscriber) {
          this.playerEventSubscriber.unsubscribe();
        }
        this.playerEventSubscriber = this.adultService.adultEvent.subscribe(response => {
          if (response === AdultCheckState.SUCCESS) {
            this.setNowPlayingForAndroid(channel, this.contentPoster);
            this.playUrl(res.stream_url);
            this.playerController.events.next(PlayerEvents.PLAYER_VALID_PIN);
          }
          if (response === AdultCheckState.CANCELED) {
            this.playerController.events.next(PlayerEvents.PLAYER_VALID_PIN);
          }
        });
      } else {
        this.setNowPlayingForAndroid(channel, this.contentPoster);
        this.playUrl(res.stream_url);
      }
    }).catch((err) => {
      this.errorHandling(err.status);
    });
  }

  private setNowPlayingForAndroid(channel: Channel, poster: string): void {
    if (environment.platform === 'android') {
      NowPlaying.set({
        artwork: poster,
        albumTitle: channel.name,
        persistentID: channel.channel_id,
        playbackDuration: 500,
        title: channel.name
      });
    }
  }

  private playTvshow(id: string) {
    if (this.dataService.activeTvshow && this.dataService.activeTvshow.cover) {
      this.contentPoster = this.dataService.activeTvshow.cover;
    }
    this.loaderService.startLoading(1);
    this.dataService.getTvshowStream(id)
      .then(res => this.playUrl(res.stream_url))
      .catch(err => this.errorHandling(err.status))
      .finally(() => this.loaderService.loadFinished());
  }

  private errorHandling(statusCode: number): void {
    this.isPlayerHaveError = true;
    switch (statusCode) {
      case 500:
        this.playerController.events.next(PlayerEvents.PLAYER_ERROR_SERVER);
        break;

      case 403:
        this.playerController.events.next(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION);
        break;
    }
  }

  private playVideo(id: number): void {
    this.contentPoster = this.dataService.activeVideo.cover;
    this.loaderService.startLoading(1);
    this.dataService.getVideoStream(id)
      .then(res => this.playUrl(res.stream_url))
      .catch(err => this.errorHandling(err.status))
      .finally(() => this.loaderService.loadFinished());
  }

  private parseStreamUrl(url: string): string {
    if (url.includes('https')) {
      return url;
    } else {
      if (url.includes('wowza2')) {
        return url;
      } else {
        let newUrl = url.replace('http', 'https');
        newUrl = newUrl.replace(':82', '');
        return newUrl;
      }
    }
  }

  private onPlayerStateChange(): void {
    this.playerController.events.next(PlayerEvents.PLAYER_READY);
  }

  private onPlayerPause(): void {
    this.playerController.events.next(PlayerEvents.PLAYER_PAUSE);
  }

  private onPlayerPlay(): void {
    this.playerController.events.next(PlayerEvents.PLAYER_PLAY);
  }

  private playUrl(url: string) {
    this.isPlayerHaveError = false;
    const parseUrl = this.parseStreamUrl(url);
    if (environment.platform === 'android' || environment.platform === 'web') {
      if (Hls.isSupported()) {
        this.playWithHls(parseUrl);
      } else {
      this.playWithStandartHtml(parseUrl);
      }
    } else {
      this.playWithStandartHtml(parseUrl);
    }
  }

  private playWithHls(url: string): void {
    if (!this.hls) {
      this.hls = new Hls();
      this.playerController.setInstance(this.player);
      this.player.addEventListener('loadedmetadata', this.stateChangeHandler);
      this.player.addEventListener('play', this.playHandler);
      this.player.addEventListener('pause', this.pauseHandler);

      this.hls.on(Hls.Events.ERROR, (_, data) => {
        if (data.fatal) {
          switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
              this.playerController.events.next(PlayerEvents.PLAYER_ERROR_SERVER);
              break;
            case Hls.ErrorTypes.MEDIA_ERROR:
              this.playerController.events.next(PlayerEvents.PLAYER_ERROR_MEDIA);
              break;
            default:
              break;
          }
        }
      });
    }
    this.hls.loadSource(url);
    this.hls.attachMedia(this.player);
    this.player.play();
  }

  private playWithStandartHtml(url: string): void {
    this.playerController.setInstance(this.player);
    this.stop(false);
    this.player.setAttribute('src', url);
    this.player.load();
    this.player.play();

    if (!this.isListenersIsSet) {
      this.isListenersIsSet = true;
      this.player.addEventListener('loadedmetadata', this.stateChangeHandler);
      this.player.addEventListener('play', this.playHandler);
      this.player.addEventListener('pause', this.pauseHandler);
      this.player.addEventListener('error', () => {
        if (this.player.error && this.player.error.message.length > 0) {
          this.playerController.events.next(PlayerEvents.PLAYER_ERROR_SERVER);
        }
      });
    }
  }

  private get isAuthorized(): boolean {
    return this.authService.isLogin;
  }

  /* private createVideoTag(): void {
    const video = this.renderer.createElement('video');
    this.renderer.addClass(video, 'player-video');
    this.renderer.setAttribute(video, 'poster', this.contentPoster);
    const player = document.getElementsByClassName('player')[0];
    this.renderer.appendChild(player, video);
    this.player = video;
  } */

  ngOnDestroy() {
    if (this.playerEventSubscriber) {
      this.playerEventSubscriber.unsubscribe();
    }
    if (this.channelSubscriber) {
      this.channelSubscriber.unsubscribe();
    }
    this.stop();
    this.player.removeEventListener('loadedmetadata', this.stateChangeHandler);
    this.player.removeEventListener('play', this.playHandler);
    this.player.removeEventListener('pause', this.pauseHandler);
  }

}
