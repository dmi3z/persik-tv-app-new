import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'tv-review',
    loadChildren: './pages/tv-review/tv-review.module#TvReviewPageModule'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  {
    path: 'channel-player/:id',
    loadChildren: './pages/channel-player/channel-player.module#ChannelPlayerPageModule'
  },
  {
    path: 'video-player/:type/:id',
    loadChildren: './pages/video-player/video-player.module#VideoPlayerPageModule'
  },
  {
    path: 'auth',
    loadChildren: './pages/auth/auth.module#AuthPageModule'
  },
  {
    path: 'account',
    loadChildren: './pages/account/account.module#AccountPageModule'
  },
  {
    path: 'tv-guide',
    loadChildren: './pages/tv-guide/tv-guide.module#TvGuideModule'
  },
  {
    path: 'films',
    loadChildren: './pages/films/films.module#FilmsPageModule'
  },
  {
    path: 'series',
    loadChildren: './pages/series/series.module#SeriesPageModule'
  },
  {
    path: 'cartoons',
    loadChildren: './pages/cartoons/cartoons.module#CartoonsPageModule'
  },
  {
    path: 'shows',
    loadChildren: './pages/shows/shows.module#ShowsPageModule'
  },
  {
    path: 'courses',
    loadChildren: './pages/courses/courses.module#CoursesPageModule'
  },
  {
    path: 'content-description/:type',
    loadChildren: './pages/content-description/content-description.module#ContentDescriptionPageModule'
  },
  {
    path: 'search',
    loadChildren: './pages/search/search.module#SearchPageModule'
  },
  {
    path: 'favorite',
    loadChildren: './pages/favorite/favorite.module#FavoritePageModule'
  },
  {
    path: 'audiobooks',
    loadChildren: './pages/audiobooks/audiobooks.module#AudiobooksPageModule'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled',
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
